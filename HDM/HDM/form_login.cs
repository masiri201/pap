﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDM
{
    public partial class form_login : Form
    {
        bool isTopPanelDragged = false;
        Point offset;
        public string nome;
        public string nome_passar;
        public int id_conta;
        MySqlConnection con = HDM.Main.Db_connection();
        public form_login()
        {
            InitializeComponent();
            Form1 frm = new Form1();
            frm.Close();
        }
        //
        //Métodos e funções
        //
        private void form_login_Load(object sender, EventArgs e)
        {
           
        }
    

        //Função que verifica se o login esta correto
        public void validate_login()
        {
            if (this.textBox_user.Text == "")
            {
                MyMessageBox.ShowMessage("Introduza o Utilizador!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else if (this.textBox_pass.Text == "")
            {
                MyMessageBox.ShowMessage("Introduza a senha!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Login lg = new Login(textBox_user.Text, textBox_pass.Text);
                lg.Validation();
                if (lg.EmpNo == string.Empty)
                {
                    if (MyMessageBox.ShowMessage("Dados de sessão incorretos","Erro",MessageBoxButtons.OK,MessageBoxIcon.Information) == DialogResult.Yes)
                    {

                    }
                    else
                    {
                        Form1 frm = new Form1();
                        frm.Closed += (s, args) => this.Close();
                        frm.ShowDialog();
                    }
                }
                else
                {

                    //Registo de atividade
                    Login_entry alog = new Login_entry(this.textBox_user.Text,Get_ip.GetLocalIPAddress(),Get_ip.GetExternalIpAddress());
                    alog.task("Inicio De Sessão");
                    //nome do funcionário
                    int id=lg.get_id_conta();
                    nome=lg.get_nome_conta(id);
                    //fecho do form
                    this.Hide();
                    Form1 frm = new Form1();
                    frm.Refresh();
                    frm.conta = passar_dados;
                    frm.Closed += (s, args) => this.Close();
                    frm.ShowDialog();
                }
            }
        }
        
        //retorna o ip local
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ip.ToString();
                    return ip.ToString();
                }
            }
            throw new Exception("Endereço Ip não foi encontrado!");
        }
        //retorna o ip externo
        public static string GetExternalIpAddress()
        {
            WebClient webClient = new WebClient();
            string IP = webClient.DownloadString("http://icanhazip.com/");
            return IP;
        }

        //Passa para o form1 o texto que está na textbox(Em breve para passar o nome do user que logou no programa)
        public string passar_dados
        {
            get {

                if (nome == "")
                {
                    nome_passar = "Iniciar Sessão";
                }   
                else
                {
                    nome_passar = nome;
                }
                return nome_passar;
            }
        }

        public MySqlDataAdapter Consultantdataadapter { get; private set; }

        //
        //Eventos e outras coisas
        //

        //Eventos para conseguir mover a janela(Quando o rato clica no panel de cima)
        private void TopPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isTopPanelDragged = true;
                Point pointStartPosition = this.PointToScreen(new Point(e.X, e.Y));
                offset = new Point();
                offset.X = this.Location.X - pointStartPosition.X;
                offset.Y = this.Location.Y - pointStartPosition.Y;
            }
            else
            {
                isTopPanelDragged = false;
            }
            if (e.Clicks == 2)
            {
                isTopPanelDragged = false;
                
            }
        }
        //Eventos para conseguir mover a janela(Quando o rato clica muda de posição enquanto está a cliclar no panel)
        private void TopPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (isTopPanelDragged)
            {
                if (this.WindowState == FormWindowState.Maximized)
                {
                    this.WindowState = FormWindowState.Normal;
                }
                Point newPoint = TopPanel.PointToScreen(new Point(e.X, e.Y));
                newPoint.Offset(offset);
                this.Location = newPoint;
            }
        }
        //Eventos para conseguir mover a janela(Quando o rato deixa de cliclar no panel)
        private void TopPanel_MouseUp(object sender, MouseEventArgs e)
        {
            isTopPanelDragged = false;
        }
        //Verificação do login quando se clica no butão entrar
        private void button_Entrar_Click(object sender, EventArgs e)
        {

            validate_login();

        }

        private void form_login_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                
                e.Handled = true;
                button_Entrar.PerformClick();
            }
        }

        private void textBox_user_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                button_Entrar.PerformClick();
               
            }
        }

        private void textBox_pass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                button_Entrar.PerformClick();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button_sair_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 frm = new Form1();
            frm.Refresh();
            frm.Closed += (s, args) => this.Close();
            frm.ShowDialog();
        }

        
    }
}
