﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace HDM
{
    class Get_ip
    {
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ip.ToString();
                    return ip.ToString();
                }
            }
            throw new Exception("Endereço Ip não foi encontrado!");
        }
        
        //retorna o ip externo
        public static string GetExternalIpAddress()
        {
            WebClient webClient = new WebClient();
            string IP = webClient.DownloadString("http://icanhazip.com/");
            return IP;
        }
    }
}
