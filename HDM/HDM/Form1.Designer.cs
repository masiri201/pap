﻿namespace HDM
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.MenuStrip_menu = new System.Windows.Forms.MenuStrip();
            this.opçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voltarAoMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iniciarSessãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terminarSessãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informaçõesDaContaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox_voltar = new System.Windows.Forms.PictureBox();
            this.Panel_dashboard = new PanelZ.PanelZ();
            this.toggle_menu = new System.Windows.Forms.PictureBox();
            this.customButton_Clientes = new HDM.CustomButton();
            this.customButton_Staff = new HDM.CustomButton();
            this.pictureBox_logo = new System.Windows.Forms.PictureBox();
            this.customButton_Reservar = new HDM.CustomButton();
            this.label_conta = new System.Windows.Forms.Label();
            this.Panel_display = new PanelZ.PanelZ();
            this.label_logout = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel_menuStrip = new PanelZ.PanelZ();
            this.label2 = new System.Windows.Forms.Label();
            this.TopPanel.SuspendLayout();
            this.MenuStrip_menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_voltar)).BeginInit();
            this.Panel_dashboard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toggle_menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_logo)).BeginInit();
            this.Panel_display.SuspendLayout();
            this.panel_menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.label1);
            this.TopPanel.Controls.SetChildIndex(this.label1, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Hotel Database Managment";
            // 
            // MenuStrip_menu
            // 
            this.MenuStrip_menu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MenuStrip_menu.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuStrip_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opçõesToolStripMenuItem,
            this.contaToolStripMenuItem});
            this.MenuStrip_menu.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip_menu.Name = "MenuStrip_menu";
            this.MenuStrip_menu.Size = new System.Drawing.Size(801, 31);
            this.MenuStrip_menu.TabIndex = 5;
            this.MenuStrip_menu.Text = "menuStrip1";
            // 
            // opçõesToolStripMenuItem
            // 
            this.opçõesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sairToolStripMenuItem,
            this.ajudaToolStripMenuItem,
            this.voltarAoMenuToolStripMenuItem});
            this.opçõesToolStripMenuItem.Name = "opçõesToolStripMenuItem";
            this.opçõesToolStripMenuItem.Size = new System.Drawing.Size(60, 27);
            this.opçõesToolStripMenuItem.Text = "Opções";
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.sairToolStripMenuItem.Text = "Sair";
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.ajudaToolStripMenuItem.Text = "Ajuda";
            // 
            // voltarAoMenuToolStripMenuItem
            // 
            this.voltarAoMenuToolStripMenuItem.Name = "voltarAoMenuToolStripMenuItem";
            this.voltarAoMenuToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.voltarAoMenuToolStripMenuItem.Text = "Voltar ao menu";
            // 
            // contaToolStripMenuItem
            // 
            this.contaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iniciarSessãoToolStripMenuItem,
            this.terminarSessãoToolStripMenuItem,
            this.informaçõesDaContaToolStripMenuItem});
            this.contaToolStripMenuItem.Name = "contaToolStripMenuItem";
            this.contaToolStripMenuItem.Size = new System.Drawing.Size(51, 27);
            this.contaToolStripMenuItem.Text = "Conta";
            // 
            // iniciarSessãoToolStripMenuItem
            // 
            this.iniciarSessãoToolStripMenuItem.Name = "iniciarSessãoToolStripMenuItem";
            this.iniciarSessãoToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.iniciarSessãoToolStripMenuItem.Text = "Iniciar Sessão";
            this.iniciarSessãoToolStripMenuItem.Click += new System.EventHandler(this.iniciarSessãoToolStripMenuItem_Click_1);
            // 
            // terminarSessãoToolStripMenuItem
            // 
            this.terminarSessãoToolStripMenuItem.Name = "terminarSessãoToolStripMenuItem";
            this.terminarSessãoToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.terminarSessãoToolStripMenuItem.Text = "Terminar Sessão";
            this.terminarSessãoToolStripMenuItem.Click += new System.EventHandler(this.terminarSessãoToolStripMenuItem_Click);
            // 
            // informaçõesDaContaToolStripMenuItem
            // 
            this.informaçõesDaContaToolStripMenuItem.Name = "informaçõesDaContaToolStripMenuItem";
            this.informaçõesDaContaToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.informaçõesDaContaToolStripMenuItem.Text = "Informações da conta";
            // 
            // pictureBox_voltar
            // 
            this.pictureBox_voltar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_voltar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.pictureBox_voltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox_voltar.Image = global::HDM.Properties.Resources._1484071956_arrow_back;
            this.pictureBox_voltar.Location = new System.Drawing.Point(98, 448);
            this.pictureBox_voltar.Name = "pictureBox_voltar";
            this.pictureBox_voltar.Size = new System.Drawing.Size(31, 31);
            this.pictureBox_voltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox_voltar.TabIndex = 0;
            this.pictureBox_voltar.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox_voltar, "Voltar Ao menu");
            this.pictureBox_voltar.Click += new System.EventHandler(this.pictureBox_voltar_Click);
            this.pictureBox_voltar.MouseLeave += new System.EventHandler(this.pictureBox_voltar_MouseLeave);
            this.pictureBox_voltar.MouseHover += new System.EventHandler(this.pictureBox_voltar_MouseHover);
            // 
            // Panel_dashboard
            // 
            this.Panel_dashboard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Panel_dashboard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.Panel_dashboard.BorderColor = System.Drawing.Color.Transparent;
            this.Panel_dashboard.BorderWidth = 1;
            this.Panel_dashboard.Controls.Add(this.pictureBox_voltar);
            this.Panel_dashboard.Controls.Add(this.toggle_menu);
            this.Panel_dashboard.Controls.Add(this.customButton_Clientes);
            this.Panel_dashboard.Controls.Add(this.customButton_Staff);
            this.Panel_dashboard.Controls.Add(this.pictureBox_logo);
            this.Panel_dashboard.Controls.Add(this.customButton_Reservar);
            this.Panel_dashboard.Controls.Add(this.label_conta);
            this.Panel_dashboard.Location = new System.Drawing.Point(3, 30);
            this.Panel_dashboard.Name = "Panel_dashboard";
            this.Panel_dashboard.Size = new System.Drawing.Size(193, 567);
            this.Panel_dashboard.TabIndex = 7;
            // 
            // toggle_menu
            // 
            this.toggle_menu.Image = global::HDM.Properties.Resources._1489013447_Menu2;
            this.toggle_menu.Location = new System.Drawing.Point(0, 0);
            this.toggle_menu.Name = "toggle_menu";
            this.toggle_menu.Size = new System.Drawing.Size(32, 30);
            this.toggle_menu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.toggle_menu.TabIndex = 13;
            this.toggle_menu.TabStop = false;
            this.toggle_menu.Click += new System.EventHandler(this.toggle_menu_Click);
            this.toggle_menu.MouseLeave += new System.EventHandler(this.toggle_menu_MouseLeave);
            this.toggle_menu.MouseHover += new System.EventHandler(this.toggle_menu_MouseHover);
            // 
            // customButton_Clientes
            // 
            this.customButton_Clientes.FlatAppearance.BorderSize = 0;
            this.customButton_Clientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customButton_Clientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customButton_Clientes.Image = global::HDM.Properties.Resources._1484072133_user;
            this.customButton_Clientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.customButton_Clientes.Location = new System.Drawing.Point(-3, 187);
            this.customButton_Clientes.MouseHoverColor1 = System.Drawing.Color.Yellow;
            this.customButton_Clientes.Name = "customButton_Clientes";
            this.customButton_Clientes.Size = new System.Drawing.Size(196, 47);
            this.customButton_Clientes.TabIndex = 12;
            this.customButton_Clientes.Text = "Clientes";
            this.customButton_Clientes.UseVisualStyleBackColor = true;
            this.customButton_Clientes.Click += new System.EventHandler(this.customButton_Clientes_Click);
            // 
            // customButton_Staff
            // 
            this.customButton_Staff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.customButton_Staff.FlatAppearance.BorderSize = 0;
            this.customButton_Staff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customButton_Staff.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customButton_Staff.Image = global::HDM.Properties.Resources._1483739774_key;
            this.customButton_Staff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.customButton_Staff.Location = new System.Drawing.Point(-3, 138);
            this.customButton_Staff.MouseHoverColor1 = System.Drawing.Color.Yellow;
            this.customButton_Staff.Name = "customButton_Staff";
            this.customButton_Staff.Size = new System.Drawing.Size(196, 47);
            this.customButton_Staff.TabIndex = 11;
            this.customButton_Staff.Text = "Staff";
            this.customButton_Staff.UseVisualStyleBackColor = false;
            // 
            // pictureBox_logo
            // 
            this.pictureBox_logo.Image = global::HDM.Properties.Resources.HDM_Logo;
            this.pictureBox_logo.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_logo.Name = "pictureBox_logo";
            this.pictureBox_logo.Size = new System.Drawing.Size(193, 89);
            this.pictureBox_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_logo.TabIndex = 10;
            this.pictureBox_logo.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox_logo, "Diminuir Menu");
            this.pictureBox_logo.Click += new System.EventHandler(this.pictureBox_logo_Click);
            this.pictureBox_logo.MouseLeave += new System.EventHandler(this.pictureBox_logo_MouseLeave);
            this.pictureBox_logo.MouseHover += new System.EventHandler(this.pictureBox_logo_MouseHover);
            // 
            // customButton_Reservar
            // 
            this.customButton_Reservar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.customButton_Reservar.FlatAppearance.BorderSize = 0;
            this.customButton_Reservar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customButton_Reservar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.customButton_Reservar.Image = global::HDM.Properties.Resources._1483739774_key;
            this.customButton_Reservar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.customButton_Reservar.Location = new System.Drawing.Point(-3, 90);
            this.customButton_Reservar.MouseHoverColor1 = System.Drawing.SystemColors.WindowFrame;
            this.customButton_Reservar.Name = "customButton_Reservar";
            this.customButton_Reservar.Size = new System.Drawing.Size(196, 47);
            this.customButton_Reservar.TabIndex = 8;
            this.customButton_Reservar.Text = "Reservar";
            this.customButton_Reservar.UseVisualStyleBackColor = false;
            this.customButton_Reservar.Click += new System.EventHandler(this.customButton_Reservar_Click);
            // 
            // label_conta
            // 
            this.label_conta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_conta.AutoSize = true;
            this.label_conta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_conta.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_conta.Location = new System.Drawing.Point(3, 540);
            this.label_conta.Name = "label_conta";
            this.label_conta.Size = new System.Drawing.Size(126, 24);
            this.label_conta.TabIndex = 9;
            this.label_conta.Text = "Iniciar Sessão";
            this.label_conta.Click += new System.EventHandler(this.label_conta_Click);
            // 
            // Panel_display
            // 
            this.Panel_display.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel_display.BackColor = System.Drawing.Color.White;
            this.Panel_display.BorderColor = System.Drawing.Color.Transparent;
            this.Panel_display.BorderWidth = 1;
            this.Panel_display.Controls.Add(this.label2);
            this.Panel_display.Location = new System.Drawing.Point(196, 61);
            this.Panel_display.Name = "Panel_display";
            this.Panel_display.Size = new System.Drawing.Size(801, 508);
            this.Panel_display.TabIndex = 8;
            // 
            // label_logout
            // 
            this.label_logout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_logout.AutoSize = true;
            this.label_logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label_logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_logout.Location = new System.Drawing.Point(818, 572);
            this.label_logout.Name = "label_logout";
            this.label_logout.Size = new System.Drawing.Size(153, 24);
            this.label_logout.TabIndex = 10;
            this.label_logout.Text = "Terminar Sessão";
            this.label_logout.Click += new System.EventHandler(this.label_logout_Click);
            // 
            // panel_menuStrip
            // 
            this.panel_menuStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_menuStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.panel_menuStrip.BorderColor = System.Drawing.Color.White;
            this.panel_menuStrip.BorderWidth = 1;
            this.panel_menuStrip.Controls.Add(this.MenuStrip_menu);
            this.panel_menuStrip.Location = new System.Drawing.Point(196, 30);
            this.panel_menuStrip.Name = "panel_menuStrip";
            this.panel_menuStrip.Size = new System.Drawing.Size(801, 31);
            this.panel_menuStrip.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(289, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Resultou";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 600);
            this.Controls.Add(this.panel_menuStrip);
            this.Controls.Add(this.Panel_display);
            this.Controls.Add(this.label_logout);
            this.Controls.Add(this.Panel_dashboard);
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.Controls.SetChildIndex(this.Panel_dashboard, 0);
            this.Controls.SetChildIndex(this.label_logout, 0);
            this.Controls.SetChildIndex(this.Panel_display, 0);
            this.Controls.SetChildIndex(this.TopPanel, 0);
            this.Controls.SetChildIndex(this.LeftPanel, 0);
            this.Controls.SetChildIndex(this.RightPanel, 0);
            this.Controls.SetChildIndex(this.BottomPanel, 0);
            this.Controls.SetChildIndex(this.panel_menuStrip, 0);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.MenuStrip_menu.ResumeLayout(false);
            this.MenuStrip_menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_voltar)).EndInit();
            this.Panel_dashboard.ResumeLayout(false);
            this.Panel_dashboard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.toggle_menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_logo)).EndInit();
            this.Panel_display.ResumeLayout(false);
            this.Panel_display.PerformLayout();
            this.panel_menuStrip.ResumeLayout(false);
            this.panel_menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip MenuStrip_menu;
        private PanelZ.PanelZ Panel_dashboard;
        private PanelZ.PanelZ Panel_display;
        private System.Windows.Forms.ToolStripMenuItem opçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iniciarSessãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terminarSessãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informaçõesDaContaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voltarAoMenuToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox_voltar;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Label label_conta;
        public System.Windows.Forms.Label label_logout;
        private CustomButton customButton_Reservar;
        private System.Windows.Forms.PictureBox pictureBox_logo;
        private CustomButton customButton_Clientes;
        private CustomButton customButton_Staff;
        private System.Windows.Forms.PictureBox toggle_menu;
        private PanelZ.PanelZ panel_menuStrip;
        private System.Windows.Forms.Label label2;
    }
}

