﻿namespace HDM
{
    partial class Clientes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelZ1 = new PanelZ.PanelZ();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_pesquisar = new HDM.Int32TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panelZ1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelZ1
            // 
            this.panelZ1.BorderColor = System.Drawing.Color.Transparent;
            this.panelZ1.BorderWidth = 1;
            this.panelZ1.Controls.Add(this.label2);
            this.panelZ1.Controls.Add(this.tb_pesquisar);
            this.panelZ1.Controls.Add(this.pictureBox1);
            this.panelZ1.Controls.Add(this.label1);
            this.panelZ1.Controls.Add(this.dataGridView1);
            this.panelZ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZ1.Location = new System.Drawing.Point(0, 0);
            this.panelZ1.Name = "panelZ1";
            this.panelZ1.Size = new System.Drawing.Size(805, 510);
            this.panelZ1.TabIndex = 0;
            this.panelZ1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelZ1_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.label2.Location = new System.Drawing.Point(564, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "*Escrever Apenas Números";
            // 
            // tb_pesquisar
            // 
            this.tb_pesquisar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_pesquisar.Location = new System.Drawing.Point(567, 30);
            this.tb_pesquisar.Name = "tb_pesquisar";
            this.tb_pesquisar.Size = new System.Drawing.Size(221, 22);
            this.tb_pesquisar.TabIndex = 7;
            this.tb_pesquisar.Enter += new System.EventHandler(this.tb_pesquisar_Enter);
            this.tb_pesquisar.Leave += new System.EventHandler(this.tb_pesquisar_Leave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HDM.Properties.Resources._1484072133_user;
            this.pictureBox1.Location = new System.Drawing.Point(115, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(31, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(11, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Clientes";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(16, 62);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(772, 235);
            this.dataGridView1.TabIndex = 0;
            // 
            // Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(92)))), ((int)(((byte)(99)))));
            this.Controls.Add(this.panelZ1);
            this.Name = "Clientes";
            this.Size = new System.Drawing.Size(805, 510);
            this.panelZ1.ResumeLayout(false);
            this.panelZ1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PanelZ.PanelZ panelZ1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private Int32TextBox tb_pesquisar;
        private System.Windows.Forms.Label label2;
    }
}
