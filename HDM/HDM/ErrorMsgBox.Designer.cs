﻿namespace HDM
{
    partial class ErrorMsgBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_caption = new System.Windows.Forms.Label();
            this.label_text = new System.Windows.Forms.Label();
            this.button_no = new ButtonZ.ButtonZ();
            this.button_yes = new ButtonZ.ButtonZ();
            this.buttonZ1 = new ButtonZ.ButtonZ();
            this.SuspendLayout();
            // 
            // label_caption
            // 
            this.label_caption.AutoSize = true;
            this.label_caption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_caption.ForeColor = System.Drawing.Color.White;
            this.label_caption.Location = new System.Drawing.Point(11, 9);
            this.label_caption.Name = "label_caption";
            this.label_caption.Size = new System.Drawing.Size(41, 13);
            this.label_caption.TabIndex = 1;
            this.label_caption.Text = "label1";
            // 
            // label_text
            // 
            this.label_text.AutoSize = true;
            this.label_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_text.ForeColor = System.Drawing.Color.White;
            this.label_text.Location = new System.Drawing.Point(11, 44);
            this.label_text.Name = "label_text";
            this.label_text.Size = new System.Drawing.Size(41, 13);
            this.label_text.TabIndex = 2;
            this.label_text.Text = "label1";
            // 
            // button_no
            // 
            this.button_no.DisplayText = "buttonZ2";
            this.button_no.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.button_no.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_no.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_no.ForeColor = System.Drawing.Color.White;
            this.button_no.GradientAngle = 90;
            this.button_no.Location = new System.Drawing.Point(681, 170);
            this.button_no.MouseClickColor1 = System.Drawing.Color.Yellow;
            this.button_no.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_no.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.button_no.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.button_no.Name = "button_no";
            this.button_no.Size = new System.Drawing.Size(87, 34);
            this.button_no.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.button_no.TabIndex = 4;
            this.button_no.Text = "buttonZ2";
            this.button_no.TextLocation_X = 6;
            this.button_no.TextLocation_Y = 5;
            this.button_no.Transparent1 = 250;
            this.button_no.Transparent2 = 250;
            this.button_no.UseVisualStyleBackColor = true;
            // 
            // button_yes
            // 
            this.button_yes.DisplayText = "buttonZ2";
            this.button_yes.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.button_yes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_yes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_yes.ForeColor = System.Drawing.Color.White;
            this.button_yes.GradientAngle = 90;
            this.button_yes.Location = new System.Drawing.Point(537, 170);
            this.button_yes.MouseClickColor1 = System.Drawing.Color.Yellow;
            this.button_yes.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button_yes.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.button_yes.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.button_yes.Name = "button_yes";
            this.button_yes.Size = new System.Drawing.Size(87, 34);
            this.button_yes.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.button_yes.TabIndex = 3;
            this.button_yes.Text = "buttonZ2";
            this.button_yes.TextLocation_X = 6;
            this.button_yes.TextLocation_Y = 5;
            this.button_yes.Transparent1 = 250;
            this.button_yes.Transparent2 = 250;
            this.button_yes.UseVisualStyleBackColor = true;
            this.button_yes.Click += new System.EventHandler(this.button_yes_Click);
            // 
            // buttonZ1
            // 
            this.buttonZ1.DisplayText = "x";
            this.buttonZ1.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.buttonZ1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonZ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonZ1.ForeColor = System.Drawing.Color.White;
            this.buttonZ1.GradientAngle = 90;
            this.buttonZ1.Location = new System.Drawing.Point(782, 3);
            this.buttonZ1.MouseClickColor1 = System.Drawing.Color.Yellow;
            this.buttonZ1.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buttonZ1.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.buttonZ1.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.buttonZ1.Name = "buttonZ1";
            this.buttonZ1.Size = new System.Drawing.Size(21, 20);
            this.buttonZ1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.buttonZ1.TabIndex = 0;
            this.buttonZ1.Text = "x";
            this.buttonZ1.TextLocation_X = 3;
            this.buttonZ1.TextLocation_Y = -4;
            this.buttonZ1.Transparent1 = 250;
            this.buttonZ1.Transparent2 = 250;
            this.buttonZ1.UseVisualStyleBackColor = true;
            this.buttonZ1.Click += new System.EventHandler(this.buttonZ1_Click);
            // 
            // ErrorMsgBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(806, 227);
            this.Controls.Add(this.button_no);
            this.Controls.Add(this.button_yes);
            this.Controls.Add(this.label_text);
            this.Controls.Add(this.label_caption);
            this.Controls.Add(this.buttonZ1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ErrorMsgBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ErrorMsgBox";
            this.Load += new System.EventHandler(this.ErrorMsgBox_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ButtonZ.ButtonZ buttonZ1;
        private System.Windows.Forms.Label label_caption;
        private System.Windows.Forms.Label label_text;
        private ButtonZ.ButtonZ button_yes;
        private ButtonZ.ButtonZ button_no;
    }
}