﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace HDM
{
    class Class_quarto
    {
        MySqlConnection con = HDM.Main.Db_connection();
           
            public Class_quarto()
            { }

        public MySqlDataAdapter GetAllQuartos()
        {
            try
            {
                string SQL = @"SELECT quarto.ID_quarto as 'Número de Quarto', quarto.Capacidade, quarto.num_camas_casal as 'Camas de casal', quarto.num_camas_solteiro as 'Camas de Solteiro', quarto.num_casas_de_banho as 'Casas de banho', quarto.Descricao as 'Descrição', quarto.Preco_noite as 'Preço' FROM hdmcompt_hdm.quarto";
                MySqlDataAdapter DataAdapt = new MySqlDataAdapter(SQL, con);
                return DataAdapt;
            }
            catch (Exception ex)
            {
                if (MyMessageBox.ShowMessage("Aconteceu um erro com a ligação na base de dados. Deseja analisar o erro?", "Erro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MessageBox.Show("O erro foi o seguinte -> " + ex.ToString(), "Erro!", MessageBoxButtons.OK);
                }
                return null;
            }
        }

        public MySqlDataAdapter FindQuartoById(int id)
        {
            try
            {
                string SQL = @"SELECT quarto.ID_quarto as 'Número de Quarto', quarto.Capacidade, quarto.num_camas_casal as 'Camas de casal', quarto.num_camas_solteiro as 'Camas de Solteiro', quarto.num_casas_de_banho as 'Casas de banho', quarto.Descricao as 'Descrição', quarto.Preco_noite as 'Preço' FROM hdmcompt_hdm.quarto where quarto.ID_quarto ="+id+"";
                MySqlDataAdapter DataAdapt = new MySqlDataAdapter(SQL, con);
                return DataAdapt;
            }
            catch (Exception ex)
            {
                if (MyMessageBox.ShowMessage("Aconteceu um erro com a ligação na base de dados. Deseja analisar o erro?", "Erro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MessageBox.Show("O erro foi o seguinte -> " + ex.ToString(), "Erro!", MessageBoxButtons.OK);
                }
                return null;
            }
        }
        public MySqlDataAdapter sql_executer(string sql)
        {
            try
            {
                MySqlDataAdapter mydata = new MySqlDataAdapter(sql, con);
                return mydata;
            }
            catch (Exception ex)
            {
                if(MyMessageBox.ShowMessage("Aconteceu um erro com a ligação na base de dados. analisar ver o erro?", "Erro", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    MessageBox.Show("O erro foi o seguinte -> " + ex.ToString(), "Erro!", MessageBoxButtons.OK);
                }
                return null;
            }

        }





    }
}


