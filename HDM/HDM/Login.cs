﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace HDM
{
    class Login
    {
        static string userid;
        static string password;
        static int Id_conta;
        static string nome_user;
        public string EmpNo = string.Empty;
        MySqlConnection con = HDM.Main.Db_connection();

        public Login()
        { }

        public Login(string _userid, string _password)
        {
            userid = _userid;
            password = _password;
            
        }

        public string getidinfo
        {
            get

            {
                return userid;
            }
        }

        public string getpassinfo
        {
            get
            {
                return password;
            }
        }
        public int get_id_conta()
        {
            try
            {
                string sql = @"Select conta.ID_conta from hdmcompt_hdm.conta where conta.username='" + userid + "'";
                MySqlDataAdapter query = new MySqlDataAdapter(sql, con);
                DataTable dt = new DataTable();
                query.Fill(dt);
                Id_conta = int.Parse(dt.Rows[0][0].ToString());
                return Id_conta;
            }
            catch(Exception ex)
            {
                PopUpMsg.Show("Ocorreu o seguinte erro -> " + ex.ToString(), "Erro!");
                return 0;
            }
        }
        public string get_nome_conta(int id)
        {
            try
            {
                string sql = @"Select funcionários.Nome_proprio, funcionários.ultimo_nome from hdmcompt_hdm.funcionários where funcionários.ID_conta=" + id + "";
                MySqlDataAdapter query = new MySqlDataAdapter(sql, con);
                DataTable dt = new DataTable();
                query.Fill(dt);

                StringBuilder output = new StringBuilder();
                foreach (DataRow dr in dt.Rows)
                {
                    //nome_user = nome_user + dt.Rows[0][0].ToString();
                    foreach (DataColumn col in dt.Columns)
                    {
                        output.AppendFormat("{0} ", dr[col]);
                    }
                }
                nome_user = output.ToString();
                return nome_user;
            }
            catch(Exception ex)
            {
                PopUpMsg.Show("Ocorreu o seguinte erro -> "+ex.ToString(), "Erro!");
                return null;
            }

        } 
   
        public string Validation()
        {
            try
            {
                DataTable consultanttable = new DataTable();
                string SQL = @"SELECT `ID_conta` FROM conta where 
                                `username` ='" + userid + "' AND `password` ='" + password + "'";
               

                MySqlDataAdapter Consultantdataadapter = new MySqlDataAdapter(SQL, con);
                
                    Consultantdataadapter.Fill(consultanttable);
             
                foreach (DataRow myrow in consultanttable.Rows)
                {
                    EmpNo = (myrow[0].ToString());
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erro", MessageBoxButtons.OK);
            }
            return EmpNo;
        }

    }
}