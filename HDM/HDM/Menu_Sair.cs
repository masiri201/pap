﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDM
{
    public partial class Menu_Sair : Form
    {
        Get_ip getip = new Get_ip();
        Form1 frm1 = new Form1();
        public Menu_Sair()
        {
            InitializeComponent();
            button_logout.Hide();
        }
        //volta ao formMenu
        private void button_cancelar_Click(object sender, EventArgs e)
        {
            frm1.Enabled = true;
            this.Close();
        }
        //sair
        private void button_Sair_Click(object sender, EventArgs e)
        {
            if (frm1.label_conta.Text == "Iniciar Sessão")
            {
                Application.Exit();
            }
            else
            {
                label_aviso.Text = "Termine Sessão antes de sair do programa";
                button_logout.Show();
            }
        }
        //fechar
        private void buttonZ1_Click(object sender, EventArgs e)
        {
            frm1.Enabled = true;
            this.Close();
        }
        //Logout
        private void button_logout_Click(object sender, EventArgs e)
        {
            try
            {
                //Registo de atividade
                Login_entry alog = new Login_entry(frm1.label_conta.Text, Get_ip.GetLocalIPAddress(), Get_ip.GetExternalIpAddress());
                alog.task("Logout");
                frm1.label_conta.Text = "Iniciar Sessão";
                label_aviso.Text = "Logout bem sucedido!";
            }
            catch (Exception ex)
            {
                ErrorMsgBox.Show("Ocorreu O seguinte erro -> "+ ex, "Erro!", "Ok", "Sair");
            }
        }
        
        //mexer janela
        protected override void WndProc(ref Message m)
        {
            const int wmNcHitTest = 0x84;
            const int htBottomLeft = 16;
            const int htBottomRight = 17;
            if (m.Msg == wmNcHitTest)
            {
                int x = (int)(m.LParam.ToInt64() & 0xFFFF);
                int y = (int)((m.LParam.ToInt64() & 0xFFFF0000) >> 16);
                Point pt = PointToClient(new Point(x, y));
                Size clientSize = ClientSize;
                if (pt.X >= clientSize.Width - 16 && pt.Y >= clientSize.Height - 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(IsMirrored ? htBottomLeft : htBottomRight);
                    return;
                }
            }
            base.WndProc(ref m);
        }

       
    }
}
