﻿namespace HDM
{
    partial class PopUpMsg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_header = new System.Windows.Forms.Label();
            this.label_text = new System.Windows.Forms.Label();
            this.buttonZ1 = new ButtonZ.ButtonZ();
            this.SuspendLayout();
            // 
            // label_header
            // 
            this.label_header.AutoSize = true;
            this.label_header.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_header.ForeColor = System.Drawing.Color.Black;
            this.label_header.Location = new System.Drawing.Point(644, 9);
            this.label_header.Name = "label_header";
            this.label_header.Size = new System.Drawing.Size(85, 29);
            this.label_header.TabIndex = 7;
            this.label_header.Text = "label1";
            // 
            // label_text
            // 
            this.label_text.AutoSize = true;
            this.label_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_text.ForeColor = System.Drawing.Color.White;
            this.label_text.Location = new System.Drawing.Point(12, 69);
            this.label_text.Name = "label_text";
            this.label_text.Size = new System.Drawing.Size(51, 16);
            this.label_text.TabIndex = 3;
            this.label_text.Text = "label1";
            // 
            // buttonZ1
            // 
            this.buttonZ1.DisplayText = "";
            this.buttonZ1.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.buttonZ1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonZ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonZ1.ForeColor = System.Drawing.Color.Black;
            this.buttonZ1.GradientAngle = 90;
            this.buttonZ1.Image = global::HDM.Properties.Resources._1488702500_done_01;
            this.buttonZ1.Location = new System.Drawing.Point(1197, 263);
            this.buttonZ1.MouseClickColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.buttonZ1.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.buttonZ1.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.buttonZ1.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.buttonZ1.Name = "buttonZ1";
            this.buttonZ1.Size = new System.Drawing.Size(80, 46);
            this.buttonZ1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.buttonZ1.TabIndex = 2;
            this.buttonZ1.TextLocation_X = 14;
            this.buttonZ1.TextLocation_Y = -500;
            this.buttonZ1.Transparent1 = 0;
            this.buttonZ1.Transparent2 = 0;
            this.buttonZ1.UseVisualStyleBackColor = true;
            this.buttonZ1.Click += new System.EventHandler(this.buttonZ1_Click);
            // 
            // PopUpMsg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.ClientSize = new System.Drawing.Size(1289, 321);
            this.Controls.Add(this.label_header);
            this.Controls.Add(this.label_text);
            this.Controls.Add(this.buttonZ1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PopUpMsg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PopUpMsg";
            this.Load += new System.EventHandler(this.PopUpMsg_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label_header;
        private ButtonZ.ButtonZ buttonZ1;
        private System.Windows.Forms.Label label_text;
    }
}