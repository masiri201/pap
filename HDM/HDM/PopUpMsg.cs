﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDM
{
    public partial class PopUpMsg : Form
    {
        public PopUpMsg()
        {
            InitializeComponent();
        }

        private void PopUpMsg_Load(object sender, EventArgs e)
        {

        }
        static PopUpMsg popup_msg; static DialogResult result = DialogResult.No;
        public static DialogResult Show(string text, string caption)
        {
            popup_msg = new PopUpMsg();
            popup_msg.label_text.Text = text;
            popup_msg.label_header.Text = caption;
            popup_msg.ShowDialog();
            return result;
        }
        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonZ1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        internal static void Show(string v1, string v2, string v3, string v4)
        {
            throw new NotImplementedException();
        }
    }
}
