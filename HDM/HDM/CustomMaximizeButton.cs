﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
namespace CustomMaximizeButton
{
    public class CustomMaximizeButton : System.Windows.Forms.Button
    {
        Color clr1, clr2;
        private Color color1 = Color.FromArgb(80, 80, 80);
        private Color color2 = Color.FromArgb(100, 100, 100);
        private Color m_hovercolor1 = Color.FromArgb(180, 180, 180);
        private Color m_hovercolor2 = Color.FromArgb(100, 100, 100);
        private int color1Transparent = 250;
        private int color2Transparent = 250;
        private Color clickcolor1 = Color.Yellow;
        private Color clickcolor2 = Color.FromArgb(250, 128, 0);
        private int angle = 90;
        private int textX = 4;
        private int textY = 5;

        public Color StartColor
        {
            get { return color1; }
            set { color1 = value; Invalidate(); }
        }
        public Color EndColor
        {
            get { return color2; }
            set { color2 = value; Invalidate(); }
        }
        public Color MouseHoverColor1
        {
            get { return m_hovercolor1; }
            set { m_hovercolor1 = value; Invalidate(); }
        }
        public Color MouseHoverColor2
        {
            get { return m_hovercolor2; }
            set { m_hovercolor2 = value; Invalidate(); }
        }
        public Color MouseClickColor1
        {
            get { return clickcolor1; }
            set { clickcolor1 = value; Invalidate(); }
        }
        public Color MouseClickColor2
        {
            get { return clickcolor2; }
            set { clickcolor2 = value; Invalidate(); }
        }

        public int Transparent1
        {
            get { return color1Transparent; }
            set
            {
                color1Transparent = value;
                if (color1Transparent > 255)
                {
                    color1Transparent = 255;
                    Invalidate();
                }
                else
                    Invalidate();
            }
        }

        public int Transparent2
        {
            get { return color2Transparent; }
            set
            {
                color2Transparent = value;
                if (color2Transparent > 255)
                {
                    color2Transparent = 255;
                    Invalidate();
                }
                else
                    Invalidate();
            }
        }

        public int GradientAngle
        {
            get { return angle; }
            set { angle = value; Invalidate(); }
        }

        public int Location_X
        {
            get { return textX; }
            set { textX = value; Invalidate(); }
        }
        public int Location_Y
        {
            get { return textY; }
            set { textY = value; Invalidate(); }
        }

        public CustomMaximizeButton()
        {
            this.Size = new System.Drawing.Size(31, 24);
            this.ForeColor = Color.White;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
        }
        //method mouse enter
        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            clr1 = color1;
            clr2 = color2;
            color1 = m_hovercolor1;
            color2 = m_hovercolor2;
        }
        //method mouse leave
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            color1 = clr1;
            color2 = clr2;
        }
        //method mouse click
        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (e.Clicks == 1)
            {
                base.OnMouseClick(e);
                color1 = clickcolor1;
                color2 = clickcolor2;
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            Color c1 = Color.FromArgb(color1Transparent, color1);
            Color c2 = Color.FromArgb(color2Transparent, color2);
            //drawing string & filling gradient rectangle
            Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(ClientRectangle, c1, c2, angle);
            SolidBrush frcolor = new SolidBrush(this.ForeColor);
            pe.Graphics.FillRectangle(b, ClientRectangle);

            //draw and fill thw rectangles of maximized window
            for (int i = 0; i < 2; i++)
            {
                pe.Graphics.DrawRectangle(new Pen(frcolor), textX + i + 1, textY, 10, 10);
                pe.Graphics.FillRectangle(Brushes.White, textX + 1, textY - 1, 12, 4);
            }

            b.Dispose();
        }

    }
}