﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDM
{
    public partial class Clientes : UserControl
    {
        public Clientes()
        {
            InitializeComponent();
            tb_pesquisar.ForeColor = SystemColors.GrayText;
            tb_pesquisar.Text = "Pesquisar Cliente pelo Número";
        }

       

        private void panelZ1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tb_pesquisar_Enter(object sender, EventArgs e)
        {
            if (tb_pesquisar.Text == "Pesquisar quarto por Número")
            {
                tb_pesquisar.Text = "";
                tb_pesquisar.ForeColor = SystemColors.WindowText;
            }
        }

        private void tb_pesquisar_Leave(object sender, EventArgs e)
        {
            tb_pesquisar.ForeColor = SystemColors.GrayText;
            tb_pesquisar.Text = "Pesquisar Cliente pelo Número";
        }
    }
}
