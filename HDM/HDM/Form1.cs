﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace HDM
{
    public partial class Form1 : CustomForm
    {
       
        public Form1()
        {
            InitializeComponent();
            toggle_menu.Enabled = false;
            toggle_menu.Visible = false;

            //36; 31


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Main.Db_connection();
            MenuStrip_menu.Renderer = new ToolStripProfessionalRenderer(new TestColorTable());
            label_logout.Hide();
           
            
            
        }

        //
        //Métodos e funções
        //
        
        public void logout()
        {
            Login_entry alog = new Login_entry(label_conta.Text,Get_ip.GetLocalIPAddress(),Get_ip.GetExternalIpAddress());
            alog.task("Sessão Terminada");
        }



        //Serve para mudar o texto da label para indicar se está sessão iniciada e se sim qual é a pessoa que iniciou sessão
        public string conta
        {
            set { label_conta.Text = value; }
        }
        
        //Serve para atualizar constantemente se a conta está inicada ou não(é utilizada sempre que o rato muda de sitio)
        public void atualizar_iniciar_sessao()
        {
            if (label_conta.Text=="Iniciar Sessão")
            {
                label_conta.Enabled = true;
                iniciarSessãoToolStripMenuItem.Enabled = true;
                terminarSessãoToolStripMenuItem.Enabled = false;
                informaçõesDaContaToolStripMenuItem.Enabled = false;
                label_logout.Hide();
            }
            else
            {
                
                label_conta.Enabled = false;
                iniciarSessãoToolStripMenuItem.Enabled = false;
                terminarSessãoToolStripMenuItem.Enabled = true;
                informaçõesDaContaToolStripMenuItem.Enabled = true;
                label_logout.Show();
            }
        }
        private void label_conta_Click(object sender, EventArgs e)
        {
            if (label_conta.Text != "Iniciar Sessão")
            {
                label_conta.Enabled = false;
                iniciarSessãoToolStripMenuItem.Enabled = false;


                PopUpMsg.Show("A conta já se encontra iniciada, termine sessão para entrar noutra conta!", "Aviso");
            }
            else
            {
                form_login frm_login = new form_login();
                this.Hide();
                frm_login.ShowDialog();
            }

        }
        //Verifica se A Conta está iniciada recebe como parâmetro um usercontrol para aplicar nos butoes que chamam user controls.
        public void verifica_login(UserControl user_control)
        {
            if (label_conta.Text == "Iniciar Sessão")
            {
                MyMessageBox.ShowMessage("Para aceder a esta parte inicie sessão!", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                Panel_display.Controls.Clear();
                Panel_display.Refresh();
                Panel_display.Controls.Add(user_control);
            }
        }

        public void clear_Panel()
        {
            Panel_display.Controls.Clear();
            Panel_clear pln = new Panel_clear();
            pln.Dock = DockStyle.Fill;
            Panel_display.Refresh();
            Panel_display.Controls.Add(pln);
        }

        //
        //área dos eventos
        //

        //Abre o form de inicio de sessão
        private void iniciarSessãoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (label_conta.Text != "Iniciar Sessão")
            {
                label_conta.Enabled = false;
                PopUpMsg.Show("A conta já se encontra iniciada, termine sessão para entrar noutra conta!", "Aviso");
            }
            else
            {
                form_login frm_login = new form_login();
                this.Hide();
                frm_login.ShowDialog();
            }
        }
        //Limpa o panel do display chamando um user control que imita o estado normal do panel
        
        
        //Butão que chama o método para limpar o panel
        private void button_Voltar_Click(object sender, EventArgs e)
        {
            clear_Panel();
        }
        //Abre o form de inicio de sessão
        
    //muda a cor da picture box para indicar ao utilizador que pode clicar na picture box
    private void pictureBox_voltar_MouseHover(object sender, EventArgs e)
        {
            Color azul_claro = Color.FromArgb(107, 193, 255);
            pictureBox_voltar.BackColor = azul_claro;
        }

        private void pictureBox_voltar_MouseLeave(object sender, EventArgs e)
        {
            Color azul = Color.FromArgb(76, 171, 206);
            pictureBox_voltar.BackColor = Color.Transparent;
            pictureBox_voltar.Refresh();
        }



        private void pictureBox_voltar_Click(object sender, EventArgs e)
        {
            Panel_display.Controls.Clear();
            Panel_clear pln = new Panel_clear();
            pln.Dock = DockStyle.Fill;
            Panel_display.Refresh();
            Panel_display.Controls.Add(pln);
        }
        
        private void terminarSessãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (label_conta.Text=="Iniciar Sessão")
            {
                label_conta.Enabled = true;
                PopUpMsg.Show("Voçê ainda não iniciou Sessão!", "Aviso");
            }
            else
            {
                logout();
                clear_Panel();
                label_conta.Text = "Iniciar Sessão";

            }
        }
        //Sempre que o rato se mexe ele verifica se a conta esta ligada
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            atualizar_iniciar_sessao();
        }

        private void label_logout_Click(object sender, EventArgs e)
        {
            if (label_conta.Text == "Iniciar Sessão")
            {
                label_conta.Enabled = true;
                PopUpMsg.Show("Voçê ainda não iniciou Sessão!", "Aviso");
            }
            else
            {
                logout();
                clear_Panel();
                label_conta.Text = "Iniciar Sessão";

            }
        }

        private void customButton_Reservar_Click(object sender, EventArgs e)
        {
            Us_reservar reservas = new Us_reservar();
            reservas.Dock = DockStyle.Fill;
            verifica_login(reservas);
        }

        private void customButton_Clientes_Click(object sender, EventArgs e)
        {
            Clientes us_clientes = new Clientes();
            us_clientes.Dock = DockStyle.Fill;
            verifica_login(us_clientes);
        }

        private void toggle_menu_Click(object sender, EventArgs e)
        {
            label_conta.Enabled = true;
            label_conta.Visible = true;
            toggle_menu.Enabled = false;
            toggle_menu.Visible = false;
            pictureBox_logo.Visible = true;
            pictureBox_logo.Enabled = true;
            Panel_dashboard.Width = 193;
            panel_menuStrip.Location = new Point(196, 30);
            panel_menuStrip.Width = 801;
            Panel_display.Location = new Point(196, 61);
            Panel_display.Width = 801;



        }

        private void pictureBox_logo_Click(object sender, EventArgs e)
        {
            label_conta.Enabled = false;
            label_conta.Visible = false;
            toggle_menu.Enabled = true;
            toggle_menu.Visible = true;
            pictureBox_logo.Visible = false;
            pictureBox_logo.Enabled = false;
            Panel_dashboard.Width = 32;
            Panel_display.Location = new Point(37, 61);
            Panel_display.Width = 962;
            panel_menuStrip.Location = new Point(36, 30);
            panel_menuStrip.Width = 962;
        }

        public Color cor_highlight = Color.FromArgb(38, 166, 230);
        Color Azul = Color.FromArgb(76, 171, 206);

        private void pictureBox_logo_MouseHover(object sender, EventArgs e)
        {
            pictureBox_logo.BackColor = cor_highlight;
        }

        private void pictureBox_logo_MouseLeave(object sender, EventArgs e)
        {
            pictureBox_logo.BackColor = Azul;
        }

        private void toggle_menu_MouseHover(object sender, EventArgs e)
        {
            toggle_menu.BackColor = cor_highlight;
        }

        private void toggle_menu_MouseLeave(object sender, EventArgs e)
        {
            toggle_menu.BackColor = Azul;
        }
    }
}

//Color table para mudar as cores da menu strip
public class TestColorTable : ProfessionalColorTable
{
    Color branco_fundo = Color.FromArgb(234, 240, 242);
    Color Azul = Color.FromArgb(76, 171, 206);


    public override Color MenuItemSelected
    {
        get { return Azul; }
    }

    public override Color MenuItemBorder
    {
        get { return branco_fundo; }
    }

    public override Color MenuItemSelectedGradientBegin
    {
        get { return branco_fundo; }
    }

    public override Color MenuItemSelectedGradientEnd
    {
        get { return Azul; }
    }

    public override Color MenuItemPressedGradientBegin
    {
        get { return branco_fundo; }
    }

    public override Color MenuItemPressedGradientEnd
    {
        get { return Azul; }
    }
    public override Color ButtonPressedBorder
    {
        get
        { return Azul; }
    }

    public override Color MenuStripGradientBegin
    {
        get
        { return Azul; }
    }
    public override Color MenuStripGradientEnd
    {
        get
        { return branco_fundo; }
    }
    public override Color MenuItemPressedGradientMiddle
    {
        get
        {
            return branco_fundo;
        }
    }
    public override Color MenuBorder
    {
        get
        { return Azul; }
    }
    
    

}


