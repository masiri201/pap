﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace HDM
{
    public partial class Us_reservar : UserControl
    {
        MySqlConnection con = HDM.Main.Db_connection();
        Class_quarto class_quarto=new Class_quarto();
        public Us_reservar()
        {
            InitializeComponent();
            radioButton_textbox.Select();

        }

        
        //Atualizar normal(por agora)
        private void button_atualizar_Click(object sender, EventArgs e)
        {
            MySqlDataAdapter mydata = class_quarto.GetAllQuartos();
            DataSet ds = new DataSet();
            mydata.Fill(ds);
            quartos_display.DataSource = ds.Tables[0];
            quartos_display.Refresh();


        }

        public void load_capacidade_cb()
        {

        }

        private void radioButton_textbox_CheckedChanged(object sender, EventArgs e)
        {
            groupBox_prefer.Enabled = false;
            
        }

        private void radioButton_pref_CheckedChanged(object sender, EventArgs e)
        {
           
            groupBox_prefer.Enabled = true;
        }

        private void quartos_display_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
         
        }


        private void panelZ1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textbox_pesquisar_KeyDown(object sender, KeyEventArgs e)
        {
            //int x = Convert.ToInt32(this.textbox_pesquisar.Text);
            //MySqlDataAdapter mydata = class_quarto.FindQuartoById(x);
            //DataSet ds = new DataSet();
            //mydata.Fill(ds);
            //quartos_display.DataSource = ds.Tables[0];
            //quartos_display.Refresh();

        }

        private void btn_procurar_Click(object sender, EventArgs e)
        {
            int x = Convert.ToInt32(this.textbox_pesquisar.Text);
            MySqlDataAdapter mydata = class_quarto.FindQuartoById(x);
            DataSet ds = new DataSet();
            mydata.Fill(ds);
            quartos_display.DataSource = ds.Tables[0];
            quartos_display.Refresh();
        }

        private void textbox_pesquisar_Enter(object sender, EventArgs e)
        {
             
        }

        private void textbox_pesquisar_Leave(object sender, EventArgs e)
        {

        }

        private void btn_preferencias_Click(object sender, EventArgs e)
        {
            if (combo_tipo_quarto.Text == "Misto")
            {
                MySqlDataAdapter mydata = class_quarto.sql_executer(@"SELECT quarto.ID_quarto as 'Número de Quarto', quarto.Capacidade, quarto.num_camas_casal as 'Camas de casal', quarto.num_camas_solteiro as 'Camas de Solteiro', quarto.num_casas_de_banho as 'Casas de banho', quarto.Descricao as 'Descrição', quarto.Preco_noite as 'Preço' FROM hdmcompt_hdm.quarto where quarto.num_camas_casal>0 and quarto.num_camas_solteiro>0");
                DataSet ds = new DataSet();
                mydata.Fill(ds);
                quartos_display.DataSource = ds.Tables[0];
                quartos_display.Refresh();
            }
            else if(combo_tipo_quarto.Text == "Solteiro")
            {
                MySqlDataAdapter mydata = class_quarto.sql_executer(@"SELECT quarto.ID_quarto as 'Número de Quarto', quarto.Capacidade, quarto.num_camas_solteiro as 'Camas de Solteiro', quarto.num_casas_de_banho as 'Casas de banho', quarto.Descricao as 'Descrição', quarto.Preco_noite as 'Preço' FROM hdmcompt_hdm.quarto where quarto.num_camas_casal=0 and quarto.num_camas_solteiro>0");
                DataSet ds = new DataSet();
                mydata.Fill(ds);
                quartos_display.DataSource = ds.Tables[0];
                quartos_display.Refresh();
            }
            else if (combo_tipo_quarto.Text == "Casal")
            {
                MySqlDataAdapter mydata = class_quarto.sql_executer(@"SELECT quarto.ID_quarto as 'Número de Quarto', quarto.Capacidade, quarto.num_camas_casal as 'Camas de casal', quarto.num_casas_de_banho as 'Casas de banho', quarto.Descricao as 'Descrição', quarto.Preco_noite as 'Preço' FROM hdmcompt_hdm.quarto where quarto.num_camas_casal>0 and quarto.num_camas_solteiro=0");
                DataSet ds = new DataSet();
                mydata.Fill(ds);
                quartos_display.DataSource = ds.Tables[0];
                quartos_display.Refresh();
            }

        }
    }
}
