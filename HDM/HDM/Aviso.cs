﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDM
{
    public partial class Aviso : Form
    {
        public Aviso()
        {
            InitializeComponent();
        }

        private void Aviso_Load(object sender, EventArgs e)
        {

        }
        static Aviso aviso_box; static DialogResult result = DialogResult.No;
        public static DialogResult Show(string text, string caption)
        {
            aviso_box = new Aviso();
            aviso_box.label_text.Text = text;
            aviso_box.label_caption.Text = caption;
            aviso_box.ShowDialog();
            return result;
        }

        private void buttonZ1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
