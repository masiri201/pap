﻿namespace HDM
{
    partial class Us_reservar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelZ1 = new PanelZ.PanelZ();
            this.btn_procurar = new ButtonZ.ButtonZ();
            this.textbox_pesquisar = new HDM.Int32TextBox();
            this.radioButton_pref = new System.Windows.Forms.RadioButton();
            this.button_atualizar = new ButtonZ.ButtonZ();
            this.radioButton_textbox = new System.Windows.Forms.RadioButton();
            this.groupBox_prefer = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_capacidade = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.int32TextBox2 = new HDM.Int32TextBox();
            this.int32TextBox1 = new HDM.Int32TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.combo_tipo_quarto = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_preferencias = new ButtonZ.ButtonZ();
            this.label1 = new System.Windows.Forms.Label();
            this.quartos_display = new System.Windows.Forms.DataGridView();
            this.panelZ1.SuspendLayout();
            this.groupBox_prefer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quartos_display)).BeginInit();
            this.SuspendLayout();
            // 
            // panelZ1
            // 
            this.panelZ1.BackColor = System.Drawing.Color.White;
            this.panelZ1.BorderColor = System.Drawing.Color.Transparent;
            this.panelZ1.BorderWidth = 1;
            this.panelZ1.Controls.Add(this.btn_procurar);
            this.panelZ1.Controls.Add(this.textbox_pesquisar);
            this.panelZ1.Controls.Add(this.radioButton_pref);
            this.panelZ1.Controls.Add(this.button_atualizar);
            this.panelZ1.Controls.Add(this.radioButton_textbox);
            this.panelZ1.Controls.Add(this.groupBox_prefer);
            this.panelZ1.Controls.Add(this.pictureBox1);
            this.panelZ1.Controls.Add(this.btn_preferencias);
            this.panelZ1.Controls.Add(this.label1);
            this.panelZ1.Controls.Add(this.quartos_display);
            this.panelZ1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZ1.Location = new System.Drawing.Point(0, 0);
            this.panelZ1.Name = "panelZ1";
            this.panelZ1.Size = new System.Drawing.Size(805, 510);
            this.panelZ1.TabIndex = 0;
            this.panelZ1.Paint += new System.Windows.Forms.PaintEventHandler(this.panelZ1_Paint);
            // 
            // btn_procurar
            // 
            this.btn_procurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_procurar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.btn_procurar.DisplayText = "Procurar";
            this.btn_procurar.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.btn_procurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_procurar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_procurar.ForeColor = System.Drawing.Color.Black;
            this.btn_procurar.GradientAngle = 90;
            this.btn_procurar.Location = new System.Drawing.Point(691, 31);
            this.btn_procurar.MouseClickColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btn_procurar.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btn_procurar.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.btn_procurar.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.btn_procurar.Name = "btn_procurar";
            this.btn_procurar.Size = new System.Drawing.Size(89, 26);
            this.btn_procurar.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.btn_procurar.TabIndex = 15;
            this.btn_procurar.Text = "Procurar";
            this.btn_procurar.TextLocation_X = -500;
            this.btn_procurar.TextLocation_Y = 5;
            this.btn_procurar.Transparent1 = 0;
            this.btn_procurar.Transparent2 = 0;
            this.btn_procurar.UseVisualStyleBackColor = false;
            this.btn_procurar.Click += new System.EventHandler(this.btn_procurar_Click);
            // 
            // textbox_pesquisar
            // 
            this.textbox_pesquisar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textbox_pesquisar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textbox_pesquisar.Location = new System.Drawing.Point(431, 33);
            this.textbox_pesquisar.Name = "textbox_pesquisar";
            this.textbox_pesquisar.Size = new System.Drawing.Size(254, 24);
            this.textbox_pesquisar.TabIndex = 13;
            this.textbox_pesquisar.Enter += new System.EventHandler(this.textbox_pesquisar_Enter);
            this.textbox_pesquisar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textbox_pesquisar_KeyDown);
            this.textbox_pesquisar.Leave += new System.EventHandler(this.textbox_pesquisar_Leave);
            // 
            // radioButton_pref
            // 
            this.radioButton_pref.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_pref.AutoSize = true;
            this.radioButton_pref.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_pref.ForeColor = System.Drawing.Color.Black;
            this.radioButton_pref.Location = new System.Drawing.Point(12, 324);
            this.radioButton_pref.Name = "radioButton_pref";
            this.radioButton_pref.Size = new System.Drawing.Size(216, 20);
            this.radioButton_pref.TabIndex = 12;
            this.radioButton_pref.TabStop = true;
            this.radioButton_pref.Text = "Pesquisar Por Preferências";
            this.radioButton_pref.UseVisualStyleBackColor = true;
            this.radioButton_pref.CheckedChanged += new System.EventHandler(this.radioButton_pref_CheckedChanged);
            // 
            // button_atualizar
            // 
            this.button_atualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_atualizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.button_atualizar.DisplayText = "Mostrar Todos";
            this.button_atualizar.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.button_atualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_atualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_atualizar.ForeColor = System.Drawing.Color.Black;
            this.button_atualizar.GradientAngle = 90;
            this.button_atualizar.Location = new System.Drawing.Point(674, 437);
            this.button_atualizar.MouseClickColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.button_atualizar.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.button_atualizar.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.button_atualizar.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.button_atualizar.Name = "button_atualizar";
            this.button_atualizar.Size = new System.Drawing.Size(115, 52);
            this.button_atualizar.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.button_atualizar.TabIndex = 7;
            this.button_atualizar.Text = "Mostrar Todos";
            this.button_atualizar.TextLocation_X = -500;
            this.button_atualizar.TextLocation_Y = 5;
            this.button_atualizar.Transparent1 = 0;
            this.button_atualizar.Transparent2 = 0;
            this.button_atualizar.UseVisualStyleBackColor = false;
            this.button_atualizar.Click += new System.EventHandler(this.button_atualizar_Click);
            // 
            // radioButton_textbox
            // 
            this.radioButton_textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButton_textbox.AutoSize = true;
            this.radioButton_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_textbox.ForeColor = System.Drawing.Color.Black;
            this.radioButton_textbox.Location = new System.Drawing.Point(431, 7);
            this.radioButton_textbox.Name = "radioButton_textbox";
            this.radioButton_textbox.Size = new System.Drawing.Size(210, 20);
            this.radioButton_textbox.TabIndex = 11;
            this.radioButton_textbox.TabStop = true;
            this.radioButton_textbox.Text = "Usar Pesquisa por número";
            this.radioButton_textbox.UseVisualStyleBackColor = true;
            this.radioButton_textbox.CheckedChanged += new System.EventHandler(this.radioButton_textbox_CheckedChanged);
            // 
            // groupBox_prefer
            // 
            this.groupBox_prefer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_prefer.Controls.Add(this.label6);
            this.groupBox_prefer.Controls.Add(this.cb_capacidade);
            this.groupBox_prefer.Controls.Add(this.label5);
            this.groupBox_prefer.Controls.Add(this.label4);
            this.groupBox_prefer.Controls.Add(this.label3);
            this.groupBox_prefer.Controls.Add(this.int32TextBox2);
            this.groupBox_prefer.Controls.Add(this.int32TextBox1);
            this.groupBox_prefer.Controls.Add(this.label2);
            this.groupBox_prefer.Controls.Add(this.combo_tipo_quarto);
            this.groupBox_prefer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_prefer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.groupBox_prefer.Location = new System.Drawing.Point(12, 347);
            this.groupBox_prefer.Name = "groupBox_prefer";
            this.groupBox_prefer.Size = new System.Drawing.Size(655, 150);
            this.groupBox_prefer.TabIndex = 9;
            this.groupBox_prefer.TabStop = false;
            this.groupBox_prefer.Text = "Preferências";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(372, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 18);
            this.label6.TabIndex = 23;
            this.label6.Text = "Capacidade";
            // 
            // cb_capacidade
            // 
            this.cb_capacidade.FormattingEnabled = true;
            this.cb_capacidade.Items.AddRange(new object[] {
            "Misto",
            "Solteiro",
            "Casal"});
            this.cb_capacidade.Location = new System.Drawing.Point(375, 54);
            this.cb_capacidade.Name = "cb_capacidade";
            this.cb_capacidade.Size = new System.Drawing.Size(104, 26);
            this.cb_capacidade.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(243, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 18);
            this.label5.TabIndex = 21;
            this.label5.Text = "Preço";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(239, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 18);
            this.label4.TabIndex = 20;
            this.label4.Text = "Máximo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(239, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 18);
            this.label3.TabIndex = 19;
            this.label3.Text = "Mínimo";
            // 
            // int32TextBox2
            // 
            this.int32TextBox2.Location = new System.Drawing.Point(242, 107);
            this.int32TextBox2.Name = "int32TextBox2";
            this.int32TextBox2.Size = new System.Drawing.Size(100, 24);
            this.int32TextBox2.TabIndex = 18;
            // 
            // int32TextBox1
            // 
            this.int32TextBox1.Location = new System.Drawing.Point(242, 56);
            this.int32TextBox1.Name = "int32TextBox1";
            this.int32TextBox1.Size = new System.Drawing.Size(100, 24);
            this.int32TextBox1.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 18);
            this.label2.TabIndex = 8;
            this.label2.Text = "Quarto:";
            // 
            // combo_tipo_quarto
            // 
            this.combo_tipo_quarto.FormattingEnabled = true;
            this.combo_tipo_quarto.Items.AddRange(new object[] {
            "Misto",
            "Solteiro",
            "Casal"});
            this.combo_tipo_quarto.Location = new System.Drawing.Point(9, 68);
            this.combo_tipo_quarto.Name = "combo_tipo_quarto";
            this.combo_tipo_quarto.Size = new System.Drawing.Size(154, 26);
            this.combo_tipo_quarto.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::HDM.Properties.Resources._1483739774_key;
            this.pictureBox1.Location = new System.Drawing.Point(210, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(31, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // btn_preferencias
            // 
            this.btn_preferencias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_preferencias.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.btn_preferencias.DisplayText = "Buscar Preferências";
            this.btn_preferencias.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.btn_preferencias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_preferencias.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_preferencias.ForeColor = System.Drawing.Color.Black;
            this.btn_preferencias.GradientAngle = 90;
            this.btn_preferencias.Location = new System.Drawing.Point(673, 372);
            this.btn_preferencias.MouseClickColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btn_preferencias.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.btn_preferencias.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.btn_preferencias.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.btn_preferencias.Name = "btn_preferencias";
            this.btn_preferencias.Size = new System.Drawing.Size(115, 52);
            this.btn_preferencias.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.btn_preferencias.TabIndex = 16;
            this.btn_preferencias.Text = "Buscar Preferências";
            this.btn_preferencias.TextLocation_X = -500;
            this.btn_preferencias.TextLocation_Y = 5;
            this.btn_preferencias.Transparent1 = 0;
            this.btn_preferencias.Transparent2 = 0;
            this.btn_preferencias.UseVisualStyleBackColor = false;
            this.btn_preferencias.Click += new System.EventHandler(this.btn_preferencias_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            this.label1.Location = new System.Drawing.Point(7, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Reservar Quartos";
            // 
            // quartos_display
            // 
            this.quartos_display.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.quartos_display.BackgroundColor = System.Drawing.Color.White;
            this.quartos_display.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.quartos_display.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.quartos_display.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.quartos_display.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.quartos_display.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.quartos_display.GridColor = System.Drawing.Color.White;
            this.quartos_display.Location = new System.Drawing.Point(12, 64);
            this.quartos_display.MultiSelect = false;
            this.quartos_display.Name = "quartos_display";
            this.quartos_display.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.quartos_display.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.quartos_display.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(171)))), ((int)(((byte)(206)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.quartos_display.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.quartos_display.Size = new System.Drawing.Size(768, 254);
            this.quartos_display.TabIndex = 0;
            this.quartos_display.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.quartos_display_CellContentClick);
            // 
            // Us_reservar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelZ1);
            this.Name = "Us_reservar";
            this.Size = new System.Drawing.Size(805, 510);
            this.panelZ1.ResumeLayout(false);
            this.panelZ1.PerformLayout();
            this.groupBox_prefer.ResumeLayout(false);
            this.groupBox_prefer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quartos_display)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PanelZ.PanelZ panelZ1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView quartos_display;
        private ButtonZ.ButtonZ button_atualizar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox_prefer;
        private System.Windows.Forms.RadioButton radioButton_pref;
        private System.Windows.Forms.RadioButton radioButton_textbox;
        private Int32TextBox textbox_pesquisar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox combo_tipo_quarto;
        private ButtonZ.ButtonZ btn_procurar;
        private ButtonZ.ButtonZ btn_preferencias;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cb_capacidade;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Int32TextBox int32TextBox2;
        private Int32TextBox int32TextBox1;
    }
}
