﻿namespace HDM
{
    partial class Panel_clear
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_clear_panel = new PanelZ.PanelZ();
            this.SuspendLayout();
            // 
            // panel_clear_panel
            // 
            this.panel_clear_panel.BackColor = System.Drawing.Color.White;
            this.panel_clear_panel.BorderColor = System.Drawing.Color.Transparent;
            this.panel_clear_panel.BorderWidth = 1;
            this.panel_clear_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_clear_panel.Location = new System.Drawing.Point(0, 0);
            this.panel_clear_panel.Name = "panel_clear_panel";
            this.panel_clear_panel.Size = new System.Drawing.Size(805, 510);
            this.panel_clear_panel.TabIndex = 0;
            // 
            // Panel_clear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel_clear_panel);
            this.Name = "Panel_clear";
            this.Size = new System.Drawing.Size(805, 510);
            this.ResumeLayout(false);

        }

        #endregion

        private PanelZ.PanelZ panel_clear_panel;
    }
}
