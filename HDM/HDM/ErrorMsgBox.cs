﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDM
{
    public partial class ErrorMsgBox : Form
    {
        public ErrorMsgBox()
        {
            InitializeComponent();
        }

        private void ErrorMsgBox_Load(object sender, EventArgs e)
        {

        }
        static ErrorMsgBox ErrorMsg; static DialogResult result = DialogResult.No;
        public static DialogResult Show(string text, string caption, string btnyes, string btnNo)
        {
            ErrorMsg = new ErrorMsgBox();
            ErrorMsg.label_text.Text = text;
            ErrorMsg.label_caption.Text = caption;
            ErrorMsg.button_yes.Text = btnyes;
            ErrorMsg.button_no.Text = btnNo;
            ErrorMsg.ShowDialog();
            return result;
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        private void button_yes_Click(object sender, EventArgs e)
        {
            result = DialogResult.Yes; ErrorMsg.Close();
        }

        private void buttonZ1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
    }
}
