﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDM
{
    public partial class FrmMessageOk : Form
    {
        public FrmMessageOk()
        {
            InitializeComponent();
        }

        private void FrmMessageOk_Load(object sender, EventArgs e)
        {

        }
        public Image MessageIcon
        {
            get { return pictureBox1.Image; }
            set { pictureBox1.Image = value; }
        }

        public string Caption
        {
            get { return lbl_Caption.Text; }
            set { lbl_Caption.Text = value; }
        }

        public string Message
        {
            get { return lbl_Message.Text; }
            set { lbl_Message.Text = value; }
        }
       
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }
    }
}
