﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDM
{
    public static class MyMessageBox
    {
        public static System.Windows.Forms.DialogResult ShowMessage(string Message, string Caption, System.Windows.Forms.MessageBoxButtons button,  System.Windows.Forms.MessageBoxIcon icon)
        {
            System.Windows.Forms.DialogResult dlgResult = System.Windows.Forms.DialogResult.None;
            switch (button)
            {
                case System.Windows.Forms.MessageBoxButtons.OK:
                    using (FrmMessageOk msgOk = new FrmMessageOk())
                    {
                        msgOk.Caption = Caption;
                        msgOk.Message = Message;
                        switch (icon)
                        {
                            case System.Windows.Forms.MessageBoxIcon.Information:
                                msgOk.MessageIcon = HDM.Properties.Resources.Information;
                                break;
                            case System.Windows.Forms.MessageBoxIcon.Question:
                                msgOk.MessageIcon = HDM.Properties.Resources.Question;
                                break;
                        }
                        dlgResult = msgOk.ShowDialog();
                    }
                        break;

                case System.Windows.Forms.MessageBoxButtons.YesNo:
                    using (FrmMessageYesNo msgYesNo = new FrmMessageYesNo())
                    {
                        msgYesNo.Caption = Caption;
                        msgYesNo.Message = Message;
                        switch (icon)
                        {
                            case System.Windows.Forms.MessageBoxIcon.Information:
                                msgYesNo.MessageIcon = HDM.Properties.Resources.Information;
                                break;
                            case System.Windows.Forms.MessageBoxIcon.Question:
                                msgYesNo.MessageIcon = HDM.Properties.Resources.Question;
                                break;
                        }
                        dlgResult = msgYesNo.ShowDialog();
                    }
                    break;
            }
            return dlgResult;
        }
    }
}
