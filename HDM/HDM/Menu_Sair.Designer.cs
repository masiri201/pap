﻿namespace HDM
{
    partial class Menu_Sair
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Sair = new ButtonZ.ButtonZ();
            this.button_cancelar = new ButtonZ.ButtonZ();
            this.label_aviso = new System.Windows.Forms.Label();
            this.button_logout = new ButtonZ.ButtonZ();
            this.buttonZ1 = new ButtonZ.ButtonZ();
            this.SuspendLayout();
            // 
            // button_Sair
            // 
            this.button_Sair.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Sair.DisplayText = "Sair";
            this.button_Sair.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.button_Sair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Sair.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Sair.ForeColor = System.Drawing.Color.White;
            this.button_Sair.GradientAngle = 90;
            this.button_Sair.Location = new System.Drawing.Point(95, 39);
            this.button_Sair.MouseClickColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.button_Sair.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.button_Sair.MouseHoverColor1 = System.Drawing.Color.Red;
            this.button_Sair.MouseHoverColor2 = System.Drawing.Color.Red;
            this.button_Sair.Name = "button_Sair";
            this.button_Sair.Size = new System.Drawing.Size(145, 42);
            this.button_Sair.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.button_Sair.TabIndex = 8;
            this.button_Sair.Text = "Sair";
            this.button_Sair.TextLocation_X = 35;
            this.button_Sair.TextLocation_Y = 5;
            this.button_Sair.Transparent1 = 250;
            this.button_Sair.Transparent2 = 250;
            this.button_Sair.UseVisualStyleBackColor = true;
            this.button_Sair.Click += new System.EventHandler(this.button_Sair_Click);
            // 
            // button_cancelar
            // 
            this.button_cancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_cancelar.DisplayText = "Cancelar";
            this.button_cancelar.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.button_cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_cancelar.ForeColor = System.Drawing.Color.White;
            this.button_cancelar.GradientAngle = 90;
            this.button_cancelar.Location = new System.Drawing.Point(95, 99);
            this.button_cancelar.MouseClickColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.button_cancelar.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.button_cancelar.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.button_cancelar.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.button_cancelar.Name = "button_cancelar";
            this.button_cancelar.Size = new System.Drawing.Size(145, 42);
            this.button_cancelar.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.button_cancelar.TabIndex = 9;
            this.button_cancelar.Text = "Cancelar";
            this.button_cancelar.TextLocation_X = 5;
            this.button_cancelar.TextLocation_Y = 5;
            this.button_cancelar.Transparent1 = 250;
            this.button_cancelar.Transparent2 = 250;
            this.button_cancelar.UseVisualStyleBackColor = true;
            this.button_cancelar.Click += new System.EventHandler(this.button_cancelar_Click);
            // 
            // label_aviso
            // 
            this.label_aviso.AutoSize = true;
            this.label_aviso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_aviso.Location = new System.Drawing.Point(12, 239);
            this.label_aviso.Name = "label_aviso";
            this.label_aviso.Size = new System.Drawing.Size(0, 16);
            this.label_aviso.TabIndex = 10;
            // 
            // button_logout
            // 
            this.button_logout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_logout.DisplayText = "Logout";
            this.button_logout.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.button_logout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_logout.ForeColor = System.Drawing.Color.White;
            this.button_logout.GradientAngle = 90;
            this.button_logout.Location = new System.Drawing.Point(95, 173);
            this.button_logout.MouseClickColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.button_logout.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.button_logout.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.button_logout.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(173)))), ((int)(((byte)(66)))));
            this.button_logout.Name = "button_logout";
            this.button_logout.Size = new System.Drawing.Size(145, 42);
            this.button_logout.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.button_logout.TabIndex = 11;
            this.button_logout.Text = "Logout";
            this.button_logout.TextLocation_X = 19;
            this.button_logout.TextLocation_Y = 5;
            this.button_logout.Transparent1 = 250;
            this.button_logout.Transparent2 = 250;
            this.button_logout.UseVisualStyleBackColor = true;
            this.button_logout.Click += new System.EventHandler(this.button_logout_Click);
            // 
            // buttonZ1
            // 
            this.buttonZ1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonZ1.DisplayText = "x";
            this.buttonZ1.EndColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.buttonZ1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonZ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonZ1.ForeColor = System.Drawing.Color.White;
            this.buttonZ1.GradientAngle = 90;
            this.buttonZ1.Location = new System.Drawing.Point(306, 2);
            this.buttonZ1.MouseClickColor1 = System.Drawing.Color.Yellow;
            this.buttonZ1.MouseClickColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buttonZ1.MouseHoverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.buttonZ1.MouseHoverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(63)))), ((int)(((byte)(63)))));
            this.buttonZ1.Name = "buttonZ1";
            this.buttonZ1.Size = new System.Drawing.Size(21, 20);
            this.buttonZ1.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.buttonZ1.TabIndex = 1;
            this.buttonZ1.Text = "x";
            this.buttonZ1.TextLocation_X = 3;
            this.buttonZ1.TextLocation_Y = -4;
            this.buttonZ1.Transparent1 = 250;
            this.buttonZ1.Transparent2 = 250;
            this.buttonZ1.UseVisualStyleBackColor = true;
            this.buttonZ1.Click += new System.EventHandler(this.buttonZ1_Click);
            // 
            // Menu_Sair
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(119)))), ((int)(((byte)(11)))));
            this.ClientSize = new System.Drawing.Size(329, 298);
            this.Controls.Add(this.button_logout);
            this.Controls.Add(this.label_aviso);
            this.Controls.Add(this.button_cancelar);
            this.Controls.Add(this.button_Sair);
            this.Controls.Add(this.buttonZ1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Menu_Sair";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu_Sair";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ButtonZ.ButtonZ button_Sair;
        private ButtonZ.ButtonZ button_cancelar;
        private System.Windows.Forms.Label label_aviso;
        private ButtonZ.ButtonZ button_logout;
        private ButtonZ.ButtonZ buttonZ1;
    }
}